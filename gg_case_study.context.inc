<?php
/**
 * @file
 * gg_case_study.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function gg_case_study_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'case_study';
  $context->description = 'Random case study op de frontpage';
  $context->tag = 'casestudy';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '<front>' => '<front>',
      ),
    ),
    'user' => array(
      'values' => array(
        'anonymous user' => 'anonymous user',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-2f2e486c498d74a364d6af86a730b453' => array(
          'module' => 'views',
          'delta' => '2f2e486c498d74a364d6af86a730b453',
          'region' => 'content_4',
          'weight' => '9',
        ),
      ),
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('Random case study op de frontpage');
  t('casestudy');
  $export['case_study'] = $context;

  return $export;
}
