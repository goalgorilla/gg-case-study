<?php
/**
 * @file
 * gg_case_study.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function gg_case_study_user_default_permissions() {
  $permissions = array();

  // Exported permission: create case_study content.
  $permissions['create case_study content'] = array(
    'name' => 'create case_study content',
    'roles' => array(
      0 => 'Administrator',
      1 => 'Assistent manager',
      2 => 'Content manager',
    ),
    'module' => 'node',
  );

  // Exported permission: delete any case_study content.
  $permissions['delete any case_study content'] = array(
    'name' => 'delete any case_study content',
    'roles' => array(
      0 => 'Administrator',
      1 => 'Assistent manager',
      2 => 'Content manager',
    ),
    'module' => 'node',
  );

  // Exported permission: delete own case_study content.
  $permissions['delete own case_study content'] = array(
    'name' => 'delete own case_study content',
    'roles' => array(
      0 => 'Administrator',
      1 => 'Assistent manager',
      2 => 'Content manager',
    ),
    'module' => 'node',
  );

  // Exported permission: edit any case_study content.
  $permissions['edit any case_study content'] = array(
    'name' => 'edit any case_study content',
    'roles' => array(
      0 => 'Administrator',
      1 => 'Assistent manager',
      2 => 'Content manager',
    ),
    'module' => 'node',
  );

  // Exported permission: edit own case_study content.
  $permissions['edit own case_study content'] = array(
    'name' => 'edit own case_study content',
    'roles' => array(
      0 => 'Administrator',
      1 => 'Assistent manager',
      2 => 'Content manager',
    ),
    'module' => 'node',
  );

  return $permissions;
}
