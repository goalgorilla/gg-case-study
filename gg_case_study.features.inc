<?php
/**
 * @file
 * gg_case_study.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function gg_case_study_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  list($module, $api) = func_get_args();
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function gg_case_study_views_api() {
  return array("version" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function gg_case_study_node_info() {
  $items = array(
    'case_study' => array(
      'name' => t('Case Study'),
      'base' => 'node_content',
      'description' => t('Create case studies with this content type'),
      'has_title' => '1',
      'title_label' => t('Client name'),
      'help' => '',
    ),
  );
  return $items;
}
